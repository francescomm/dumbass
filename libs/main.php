<?php

// library

define('VERBOSE_LEVEL',2);

define('USERS_FILE','people.txt');
define('USERS_COLUMNS',array('key','kind','type','name','nick'));

define('PLACES_FILE','locations.txt');
define('PLACES__COLUMNS',array('key','kind','name'));

define('URLS_FILE','urls.txt');
define('URLS__COLUMNS',array('key','kind','name'));

define('CALLS_FILE','calls.txt');
define('CALLS_COLUMNS',array('number','name'));




class Language {

	var $commands=array(
	
	);
	
	
	var $articles=array('il'=>'the', 'la'=>'the', 'con'=>'with');
	var $initials=array('scrivi a'=>'scrivi_a', 'messaggio a'=>'messaggio_a', 'manda a'=>'manda_a', 'vai a'=>'vai_a','aggiorna dati'=>'aggiorna_dati');
	var $middles=array();
	var $finals=array('usando l\'app'=>'usando', 'con'=>'con', 'tramite'=>'con');
	var $translate=array('scrivi_a'=>'write_to', 'vai_a'=>'go_to', 'manda_a'=>'send_to', 'chiama'=>'call');
	
	function __construct() {
		$this->prepareTogethers();
    }
	
	function prepareSome(&$arr) {
		$prepared=array();
		foreach($arr as $nuple=>$one) {
			$nuple=explode(' ',$nuple);
			$prepared[$nuple[0]][]=array('ALL'=>$nuple,'SUBST'=>$one);
		}
		return $prepared;
	}
	
	function prepareTogethers() {
		$this->initials=$this->prepareSome($this->initials);
		$this->middles=$this->prepareSome($this->middles);
		$this->finals=$this->prepareSome($this->finals);
	}
	
	function replaceText($txtArr,$start=0) {
		// echo("replaceText\n");
		// print_r($txtArr);
	
		if(isset($this->initials[strtolower($txtArr[$start])])) {
			$repl=$this->initials[strtolower($txtArr[$start])];
			
			// print_r($repl);
			foreach($repl as $replOne) {
				$equal=TRUE;
				foreach($replOne['ALL'] as $key=>$val) {
					if(strtolower($txtArr[$start+$key])!=$val) {
						$equal=FALSE;
						break;
					}
				}
				if($equal) {
					array_splice($txtArr,$start,count($replOne['ALL']),$replOne['SUBST']);
					
					return $txtArr;
				}
			}
			
		}
		echo("no replacement for command ".strtolower($txtArr[$start])."\n");
		$cmd=strtolower(array_shift($txtArr));
		
		array_unshift($txtArr,$cmd);
		
		return $txtArr;
	}
	
	function translateCommand($txt) {
		return str_replace(array_keys($this->translate),array_values($this->translate),$txt);
	}
	function parseText($txt) {
		
	}
	function preParseText($txt) {
		return $this->replaceText($txt);
	}
	function command($txt) {
		
	}
}




class CliInterface extends GenericInterface {

	function interpretCommand($argv) {
		array_shift($argv);
		
		$param=explode(' ',implode(' ',$argv));
		
		return array('cmd'=>'text-cmd','param'=>$param);
		
	}
	
	function allAddresses() {
		$contactsTempFile=$this->dataFolder.'contacts.txt';
		
		$res=file_get_contents($contactsTempFile);
		if(!$res) return array();
		$res=json_decode($res,TRUE);

		$out=array();
		foreach($res as $anItem) {
			$out[strtolower($anItem['name'])]=$anItem['number'];
			
		}
		
		return $out;
	}

	function selectFromList($aList) {
		$cnt=1;
		$res=array();
		foreach($aList as $k=>$v) {
			echo("$cnt) $k ($v)\n");
			$res[$cnt]=$k; // or just use array_keys
			$cnt++;
		}
		if($title!='') echo("\n\nScegli '$title':");
		else echo("\n\nScegli:");
		$input = trim(fgets(STDIN));
		echo("\n\n");
		$input=intVal($input);
		
		if(isset($res[$input])) return $res[$input];
		else return FALSE;
	}


}

class TermuxInterface extends GenericInterface {
	// termux-tts-speak -l it - r 1.2 Confermi?
	// termux-dialog speech - t Dimmi -i Parla e dì qualcosa
	// termux-dialog sheet - t Scegli -v "uno,due,tre"

	
	
	function debugError($msg) {
		echo($msg."\n");
		// sleep(2);
	}

	function allAddresses() {
		
		$res=`termux-contact-list`;
		if(!$res) return array();
		
		$res=json_decode($res,TRUE);
		
		
		$out=array();
		foreach($res as $anItem) {
			$out[strtolower($anItem['name'])]=$anItem['number'];
			
		}
		
		return $out;
	}

	
	// if you provide a no list will return +1,0,-1 for yes, ambigous, no
	// if you do not provide a no list will return TRUE for yes, FALSE otherwise
	
	function confirm($txt,$yesList,$noList=array()) {
		$yes=explode(', ',$yesList);
	
		$res=`termux-tts-speak -l it - r 1.2 "Confermi?"`;
		$res=`termux-dialog speech - t "Confermi?" -i ($yes)`;
		if(in_array(trim(txt),$yesList)) return (count($noList)>0)?1:TRUE;
		if(in_array(trim(txt),$noList)) return -1;
		return (count($noList)>0)?0:FALSE; // Ambiguous
	}
	
	function error($msg) {
		`termux-tts-speak -l it - r 1.2 $msg`;
		echo('Speaking: '.$msg."\n");
	}
	
	function message($msg) {
		`termux-tts-speak -l it - r 1.2 $msg`;
		echo('Speaking: '.$msg."\n");
	}
	
	function askFor($speak,$title='Input vocale',$msg='Pronuncia una frase...') {
		`termux-tts-speak -l it - r 1.2 $speak`;
		`termux-dialog speech - t $title -i $msg`;
		echo('Speaking: '.$msg."\n");
	}
	
	function selectFromList($aList,$title='') {
	
	// termux-tts-speak -l it - r 1.2 Confermi?
	// termux-dialog speech - t Dimmi -i Parla e dì qualcosa
	// termux-dialog sheet - t Scegli -v "uno,due,tre"
		if($title!='') $title=' '.$title;
		$res=`termux-tts-speak -l it -r 1.2 Scegli $title.`;
		
		$res=array();
		$list='';
		$sep='';
		$cnt=0;
		$results=array();
		foreach($aList as $k=>$v) {
			$v=str_replace(',','_',$v);
			$list.=$sep.($cnt+1).") $k ($v)";
			$sep=',';
			echo("$cnt) $k ($v)\n");
			$results[$cnt]=$k; // or just use array_keys
			$cnt++;
		}
		
		$res=`termux-dialog sheet -v "$list"`;
		
		
		if(!$res) return FALSE;
		$res=json_decode($res,TRUE);

		//var_dump($res);
		if(!isset($res['code'])) return FALSE;
		if(!isset($res['index'])) return FALSE;
		
		$res=$res['index'];

		
		$input=intVal($res);
		// var_dump("$input\n");
		
		if($input<0) return FALSE;
		
		if(isset($results[$input])) return $results[$input];
		else return FALSE;
	}



}




class TaskerInterface extends TermuxInterface {

	// Have Tasker send commands as an URL
	function interpretCommand($argv) {
		$parts=$this->parseUrl($argv[1]); // via URL on first parameter
		
		if($parts['CMD']!='termux-cmd') $this->debugError("Command termux-cmd expected found ".$parts['CMD']."\n");
		
		$vars=$parts['VARS'];
	
		if(!isset($vars['cmd'])) $this->debugError("Command (cmd) expected found ".print_r($vars,TRUE)."\n");

		$cmd=$vars['cmd'];
		if(!isset($vars['param'])) $vars['param']='';
		$param=$vars['param'];
		
		return array('cmd'=>$cmd,'param'=>$param);
	}


	
	function activateTaskerProfileWithVariable($profile,$k,$v) {
	
		$kEsc=addcslashes($k, " ?&@\r\n");
		$vEsc=addcslashes($v, "\\\"\r\n");
		// echo("am broadcast --user 0 -a net.dinglish.tasker.$profile -e $varName \"$varValueEsc\"\n");
		$res=`am broadcast --user 0 -a net.dinglish.tasker.$profile -e $varName "$varValueEsc"`;
		// echo($res);
	}
	function home() {
		$this->activateTaskerProfileWithVariables('home',array());
	}
	
	function activateTaskerProfileWithVariables($profile,$vars) {
	
		$cmdVars='';
		foreach($vars as $k=>$v) {
			$kEsc=addcslashes($k, " ?&@\r\n");
			$vEsc=addcslashes($v, "\\\"\r\n");
			$cmdVars.=' -e '.$kEsc.' "'.$vEsc.'"';
		}
	
		// echo("am broadcast --user 0 -a net.dinglish.tasker.${profile}${cmdVars}\n");
		$res=`am broadcast --user 0 -a net.dinglish.tasker.${profile}${cmdVars}`;
		// echo($res);
	}
	
	function sendIntentToApp($profile,$vars=array()) {
	
		$cmdVars='';
		foreach($vars as $k=>$v) {
			$kEsc=addcslashes($k, " ?&@\r\n");
			$vEsc=addcslashes($v, "\\\"\r\n");
			$cmdVars.=' -e '.$kEsc.' "'.$vEsc.'"';
		}
	
		// echo("am broadcast --user 0 -a net.dinglish.tasker.${profile}${cmdVars}\n");
		$res=`am broadcast --user 0 -a ${profile}${cmdVars}`;
		// echo($res);
	}
	
	function sendMessageToNumber($phone,$msg,$via='sms') {
		echo("Sending message '$msg' to number $phone, via '$via'"."\n");
		
		if($via=='whatsapp') $profile='wapp_send';
		else $profile='sms_send';
		$this->home();

		$number=preg_replace('/[^0-9+]+/','',$number);
		$this->activateTaskerProfileWithVariables($profile,array(
			'number'=>$phone,
			'text'=>$msg,
		));
	}
	
	function calllNumber($phone,$via='phone') {
		echo("Calling number $phone, via '$via'"."\n");
		
		if($via!='phone') {
			echo("Calling with app '$via' is not supported yet, add code for it\n");
		}
		$this->home();

		$phone=preg_replace('/[^0-9+,pw]+/','',$phone);
		$res= `termux-telephony-call $phone`;
		
		echo($res);
	}
	
	function driveTo($addr,$isPreferiteKind='loc',$via='waze') {
	
		$isPreferite=($isPreferiteKind=='fav');
	
		if($isPreferite) $posType="$via preferite";
		else $posType="address";
	
		echo("Driving to $posType $addr, via '$via'"."\n");
		
		if($via=='maps') $profile=$isPreferite?'maps_to_fav':'maps_to_loc';
		else $profile=$isPreferite?'waze_to_fav':'waze_to_loc';

		$this->home();
		
		$this->activateTaskerProfileWithVariables($profile,array(
			'drive_location'=>$addr,
		));
	}
	function locatePlace($addr,$isPreferiteKind='loc',$via='maps') {
	
		$isPreferite=($isPreferiteKind=='fav');
	
		if($isPreferite) $posType="$via preferite";
		else $posType="address";
	
		echo("Driving to $posType $addr, via '$via'"."\n");
		
		if($via=='maps') $profile=$isPreferite?'maps_show_fav':'maps_show_loc';
		else $profile=$isPreferite?'waze_show_fav':'waze_show_loc';

		$this->home();
		
		$this->activateTaskerProfileWithVariables($profile,array(
			'drive_location'=>$addr,
		));
	}
		
	function openUrl($addr,$isPreferiteKind='loc',$via='firefox') {
		// Application favorites and bookmarks are not supported
		// Use urls.txt to add bookmarks
	
		$isPreferite=($isPreferiteKind=='fav'); // useless
	
		if($isPreferite) $posType="$via bookmark"; // useless
		else $posType="url";
	
		echo("Opening $posType $addr, via '$via'"."\n");
		
		if($via=='chrome') $profile='com.android.chrome/com.google.android.apps.chrome.Main';
		else if($via=='default') $profile='';
		else $profile='org.mozilla.firefox/org.mozilla.firefox.App';

		//$this->home();
		
		if($profile=='') {
			if(strpos($addr,'http')===0) {	// use substr here
				echo("1\n");
				$res=`termux-open-url $addr`;
			} else {
				echo("2\n");
				$res=`termux-open-url http://$addr`;
			}
		} else {
			$cmd="am start --user 0 -a android.intent.action.VIEW -n $profile -d $addr";

// $cmd="am start --user 0 -a android.intent.action.VIEW -n org.mozilla.firefox/.App -d http://google.it/search?q=pippo";


			echo($cmd);
			echo("\n");
			$res=`$cmd`;
		}
		echo($res);
		sleep(10);
		// am start --user 0 -a android.intent.action.VIEW -n org.mozilla.firefox/.App -d "$1" >/dev/null
		
		//$this->sendIntentToApp($profile,array(
		//	'drive_location'=>$addr,
		//));
	}


	
}





class GenericInterface {
	
	var $dataFolder='./';
	var $FIRST_LEVEL_DOMS=array('it','com','net','org');
	
	function __construct($dataFolder=NULL) {
		global $DATA_BASE;
		
		if($dataFolder==NULL) $dataFolder=$DATA_BASE;
		$this->dataFolder=$dataFolder;
    }
	function parseUrl($url) {
		$url=explode('?',$url,2);
		$path=explode('/',$url[0]);
		$cmd=$path[count($path)-1];
		$varChunks=explode('&',$url[1]);
		
		unset($path[count($path)-1]);
		$server=implode('/',$path);
		
		
		$vars=array();
		foreach($varChunks as $aChunk) {
			$tmp=explode('=',$aChunk);
			$vars[urldecode($tmp[0])]=urldecode($tmp[1]);
		}

		// echo("$cmd\n");
		// print_r($vars);
		
		return array(
			'SERVER'=>$server,
			'CMD'=>$cmd,
			'VARS'=>$vars,
		);
	}
	function readFileAsList($file,$cols=NULL,$col=NULL) {
		$out=array();
		$theList=file($file);
		if($col===NULL) {
		
			foreach($theList as $item) {
				if($cols!==NULL) {
					$tmp=explode("\t",trim($item));
					$new=array();
					foreach($cols as $colNum=>$aColumn) {
						$new[$aColumn]=isset($tmp[$colNum])?$tmp[$colNum]:'';
					}
					$out[]=$new;
				} else {
					$out[]=explode("\t",trim($item));
				}
			}
		} else {
			foreach($theList as $line=>$item) {
				if($cols!==NULL) {
					$colKey=NULL;
					$tmp=explode("\t",trim($item));
					$new=array();
					foreach($cols as $colNum=>$aColumn) {
						if($aColumn===$col) {
							$colKey=isset($tmp[$colNum])?$tmp[$colNum]:'unset';
						} else $new[
						$aColumn]=isset($tmp[$colNum])?$tmp[$colNum]:'';
					}
					$out[$colKey]=$new;
				} else {
					$tmp=explode("\t",trim($item));
					$colKey=$tmp[$col];
					unset($tmp[$col]);
					$out[$colKey]=array_values($tmp);
				}
			}
		}
// 		echo("readFileAsList($file,".print_r($cols,TRUE).",$col)\n");
// 		print_r($out);
		return $out;
	}
	function removeItemsFromList($list,$keys=NULL) {
		$new=array();
		foreach($list as $k=>$rec) {
			$remove=TRUE;
			foreach($keys as $kk=>$kv) {
				if($kk==='@key') {
					if($k!==$kv) {
						$temove=FALSE;
						break;
					}
				} else {
					if($rec[$kk]!=$kv) {
						$remove=FALSE;
						break;
					}
				}
			}
			if(!$remove) $new[$k]=$rec;
		}
		return $new;
	}
	function saveListAsFile($file,$list,$cols=NULL,$col=NULL) {
		if($col===NULL) {
			$out=array();
			foreach($list as $split) {
				
				if($cols!==NULL) {
					$line=array();
					foreach($cols as $colNum=>$aColumn) {
						$line[$colNum]=$split[$aColumn];
					}
					$out[]=implode("\t",$line);
				} else {
					$out[]=implode("\t",$split);
				}
			}
			file_put_contents($file,implode("\n",$out));
		} else {
			$out=array();
			foreach($list as $key=>$split) {
				
				if($cols!==NULL) {
					$line=array();
					foreach($cols as $colNum=>$aColumn) {
						if($aColumn!==$col) $line[$colNum]=$split[$aColumn];
						else $line[$colNum]=$key;
					}
					$out[]=implode("\t",$line);
				} else {
					$line=array();
					for($colNum=0;$colNum<count($split);$colNum++) {
						if($colNum!==$col) $line[$colNum]=$split[$colNum];
						else $line[$colNum]=$key;
					}
					$out[]=implode("\t",$line);
				}
			}
			file_put_contents($file,implode("\n",$out));
		}
	}
	
	function addUniqueToListFile($list,$item,$keys) {
		// ...		
	}
	function sendOneMessage($aName,$msg,$app='sms') {
		$app=strtolower($app);
		$nameNumber=$this->findNumberFromName($aName);
		if($nameNumber) {
		
			list($name,$number)=$nameNumber;
			
			$this->sendMessageToNumber($number,$msg,$app);
		} else echo("Number not chosen\n");
	}

	function callName($aName,$app='phone') {
		$app=strtolower($app);
		$nameNumber=$this->findNumberFromName($aName);
		if($nameNumber) {
			list($name,$number)=$nameNumber;
			$this->calllNumber($number,$app);
		} else echo("Number not chosen\n");
	}

	function sendMessageToNumber($phone,$msg,$via='sms') {
		echo("# Sending message '$msg' to number $phone, via '$via'"."\n");
	}
	
	function calllNumber($phone,$via='phone') {
		echo("# Calling number $phone, via '$via'"."\n");
	}

	
	function findNumberFromName($aName) {
	
		echo("Searching favs for $aName\n");
		$short=$this->findInFile($aName,USERS_FILE,USERS_COLUMNS);

		if($short!==FALSE) {
			$lookup=$short['name'];
			$kind=$short['kind']; // exact // name // value
			$type=$short['type']; // mobile // fixed
			if(isset($short['nick'])) $nick=$short['nick'];
			else $nick='';
			
		} else {
			$lookup=$aName;
			$kind='name';
			$type='unknown';
			$nick='';
		}
		
		
		if($kind==='value') {
			$name=$aName;
			if($nick!='') $name=$nick;
			$phone=$lookup;
		} else {
			$foundList=$this->findNumbersFromAddressBook($lookup,$kind);
			print_r($foundList);
		
			if(count($foundList)==0) {
				echo("none\n");
		
				$name='';
				$phone=$lookup;
			
			} else if(count($foundList)==1) {
				echo("one\n");
		
				$name=firstKey($foundList);
				$phone=firstVal($foundList);
			
			} else {
				echo("multiple\n");
				$res=$this->selectFromList($foundList);
			
				if($res!==FALSE) {
					$name=$res;
					$phone=$foundList[$res];
				
				} else return FALSE;
			
			}
		}
		
		return array($name,$phone);
		
		
	}

	function findPlace($name,$via='maps') {
		$via=strtolower($via);
		$find=$this->findPlaceFromFavs($name);
		$isPreferite=FALSE;
		
		if($find) {
			list($kind,$loc)=$find;
			if($kind==='fav') $isPreferite=TRUE;
		} else {
			$kind='loc';
			$loc=$name;
		}
		
		$this->locatePlace($loc,$kind,$via);
	}
	function goToPlace($name,$via='waze') {
		$via=strtolower($via);
		$find=$this->findPlaceFromFavs($name);
		$isPreferite=FALSE;
		
		if($find) {
			list($kind,$loc)=$find;
			if($kind==='fav') $isPreferite=TRUE;
		} else {
			$kind='loc';
			$loc=$name;
		}
		
		$this->driveTo($loc,$kind,$via);
	}
	
	function findPlaceFromFavs($aName) {
	
		echo("Searching internet fav for $aName\n");
		$found=$this->findInFile($aName,PLACES_FILE,PLACES__COLUMNS);
				
		if($found!==FALSE) return array($found['kind'],$found['name']);
		else return FALSE;
	}
	
	function searchWeb($text,$via='firefox',$engine='google.it') {
		$via=strtolower($via);
		$url='http://'.$engine.'/search?q='.urlencode($text);
	
		return $this->goToWeb($url,$via,TRUE);
	}
	
	
	function goToWeb($name,$via='firefox',$noSearch=FALSE) {
		$via=strtolower($via);
		if(!$noSearch) {
			$find=$this->findWebFromFavs($name);
			$isPreferite=FALSE;
		
			if($find) {
				list($kind,$loc)=$find;
				if($kind==='fav') $isPreferite=TRUE;
			} else {
				$kind='loc';
				$loc=$name;
			}

		
			$singleWord=FALSE;
			if(strpos($loc,'.')===FALSE) {
				$singleWord=TRUE;
				echo("Single word: $loc\n");
			}
		
			$noProto=FALSE;
			if(strpos($loc, ':')===FALSE) {
				$noProto=TRUE;
				echo("No protocol: $loc\n");
			}
		
			$noWWW=FALSE;
			if(strpos($loc, 'www')===FALSE) {
				$noWWW=TRUE;
				echo("No WWW: $loc\n");
			}
		
			if($noProto && $singleWord) foreach($this->FIRST_LEVEL_DOMS as $aDom) {
				echo("Testing 'http[s]://$loc.$aDom\n");
				if($this->testDomain('http://'.$loc.'.'.$aDom)) {
					$loc='http://'.$loc.'.'.$aDom;
					break;
				} else if($noWWW && $this->testDomain('http://www.'.$loc.'.'.$aDom)) {
					$loc='http://'.$loc.'.'.$aDom;
					break;
				}
			} else if(/* !$noProto && */ $singleWord) foreach($this->FIRST_LEVEL_DOMS as $aDom) {
		
				echo("Testing '$loc.$aDom\n");
				if($this->testUrl($loc.'.'.$aDom)) {
					$loc=$loc.'.'.$aDom;
					break;
				}
			} else {
				$parsedUrl=parse_url($loc);
				if(!$parsedUrl) return FALSE;
				if(!$parsedUrl[' scheme']==NULL) $loc='http://'.$loc;
			}		
		} else {
			$kind='loc';
			$loc=$name;
		}
		$this->openUrl($loc,$kind,$via);
	}
	
// 	function testUrl($u) {
// 		$Has200=`curl -fail -silent -I http://yourpage.com | grep HTTP | grep 200`;
// 		if(trim($Has200)!=='') return TRUE;
// 		else return FALSE;
// 	}
	
	public function testUrl($url) {
		$result = false;
		if (!filter_var($url, FILTER_VALIDATE_URL) === false) {
			if(!$this->testDomain($url)) return FALSE;
			
			$result = isDomainAvailible($url);
			
			// $getHeaders = get_headers($url);
			// $result = strpos($getHeaders[0], '200') !== false;
		}
		return $result;
	}
	
	
	public function testDomain($url) {
		
		global $fakeip;
		
		if(!isset($fakeip)) $fakeip=gethostbyname('prettysurenonexistentoasodsoijad33fucksijadspsads'.rand(0, 1000).'.dsaajofuckprovidercheating');

		$PARSED_URL = parse_url($url);
		$DOMAIN = $PARSED_URL['host'];
		$ip=gethostbyname($DOMAIN);
		
		if($ip===$fakeip) return FALSE;
		$checkIp1=!($ip===$DOMAIN);
		
		return $checkIp1;
	}
	
	function findWebFromFavs($aName) {	
		echo("Searching internet fav for $aName\n");
		$found=$this->findInFile($aName,URLS_FILE,URLS__COLUMNS);
				
		if($found!==FALSE) return array($found['kind'],$found['name']);
		else return FALSE;
	}
	
	function findInFile($aName,$fName,$columns,$key='key') {
	
		echo("Searching internet fav for $aName\n");
	
		$placesFile=$this->dataFolder.$fName;
		
		$list=$this->readFileAsList($placesFile,$columns,$key);
		// print_r($list);
		
		if(isset($list[strtolower($aName)])) {
			$short=$list[strtolower($aName)];

			return $short;
		} else return FALSE;
	}
	
	function findNumbersFromAddressBook($aName,$kind='unknown') {
	
		echo("Searching address list for $aName ($kind)\n");
		if(trim($aName)==='') return array();
		
		$aName=strtolower($aName);
		
		$addrs=$this->allAddresses();
		
		if($kind=='exact') {
			if(isset($addrs[$aName])) return array($aName=>$addrs[$aName]);
			else return array();
		} else {
			$out=array();
			foreach($addrs as $key=>$val) { // First beginning with
				if(strpos($key, $aName) === 0) {
					$out[$key]=$val;
				}
			}
			foreach($addrs as $key=>$val) { // Then containing
				$p=strpos($key, $aName);
				if($p !== false && $p!=0) $out[$key]=$val;
			}
			return $out;
		}
	}
	
	function allAddresses() {
		return array();
	}

	function debugError($msg) {
		echo($msg."\n");
	}
	
	function error($msg) {
		echo($msg."\n");
	}
	
	function message($msg) {
		echo($msg."\n");
	}
	
	function askFor($speak,$title='Input testuale',$msg='Scrivi una risposta...') {
		$cnt=1;
		$res=array();
		if($speak!='') echo("\n\n### $speak ###\n");
		if($title!='') echo("\n$title\n\n");
		if($msg!='') echo("\n$msg\n>");
		else echo("\n\nScrivi:");
		$input = trim(fgets(STDIN));
		echo("\n\n");
		
		return $input;
	}
	
	function selectFromList($aList) {
		$cnt=1;
		$res=array();
		foreach($aList as $k=>$v) {
			echo("$cnt) $k ($v)\n");
			$res[$cnt]=$k; // or just use array_keys
			$cnt++;
		}
		if($title!='') echo("\n\nScegli '$title':");
		else echo("\n\nScegli:");
		$input = trim(fgets(STDIN));
		echo("\n\n");
		$input=intVal($input);
		
		if(isset($res[$input])) return $res[$input];
		else return FALSE;
	}
	
	function feedback($msg,$verboseLevel=0) {
		if($verboseLevel<=VERBOSE_LEVEL) echo($msg."\n");
	}

	function confirm($txt,$yesList,$noList) {
		return TRUE;
	}


}


class CommandUtils {
	function findCommandChunk($txtArr,$possibleChunks,$start=0) {
		echo("findCommandChunk\n");
		// print_r($txtArr);
		
		do {
			// echo("cnt=$start\n");
			while($start < count($txtArr) && !isset($possibleChunks[$txtArr[$start]]) ) $start++;
			// echo("cnt=$start\n");
		
			if(!($start < count($txtArr))) break;
			$repl=$possibleChunks[$txtArr[$start]];
		
				
//			echo("\n\nstart\n");
// 			print_r($start);
// 			echo("\n\ntxtArr\n");
// 			print_r($txtArr);
// 			echo("\n\npossibleChunks\n");
// 			print_r($possibleChunks);
//  		echo("\n\nrepl \n");
//  		print_r($repl);
// 			echo("HERE \n\n\n");
				
			
			foreach($repl as $replOne) {
			
 				// echo("\n\nrepl \n");
 				// print_r($replOne);
 				// echo("\n\nis\n\n");
				$equal=TRUE;
				foreach($replOne['ALL'] as $key=>$val) {
					if($txtArr[$start+$key]!=$val) {
						$equal=FALSE;
						break;
					}
				}
				if($equal) {
					// echo("\n\nEQUAL\n\n");
					$bef=array_splice($txtArr,0,$start);
					$mid=array_splice($txtArr,0,count($replOne['ALL']));
					
					return array(
						'BEFORE'=>$bef,
						'CHUNK'=>$replOne['ALL'],
						'SUBST'=>$replOne['SUBST'],
						'AFTER'=>$txtArr,
					);
				}
				// echo("\n\nNOT EQUAL\n\n");

			}
			$start++;
			
		} while ($start < count($txtArr));
		
		return FALSE;
	}

	function findMultipleChunks($txtArr,$possibleChunks,$start=0) {
		echo("findMultipleChunks\n");
// 		print_r($txtArr);

		$results=array();

		do {
// 			echo("cnt=$start\n");
			while($start < count($txtArr) && !isset($possibleChunks[$txtArr[$start]]) ) $start++;
// 			echo("cnt=$start\n");
		
			if(!($start < count($txtArr))) break;
			$repl=$possibleChunks[$txtArr[$start]];
		
				
// 			echo("\n\nstart\n");
// 			print_r($start);
// 			echo("\n\ntxtArr\n");
// 			print_r($txtArr);
// 			echo("\n\npossibleChunks\n");
// 			print_r($possibleChunks);
// 			echo("\n\nrepl \n");
// 			print_r($repl);
// 			echo("HERE \n\n\n");
				
			
			foreach($repl as $replOne) {
			
//  				echo("\n\nrepl \n");
//  				print_r($replOne);
//  				echo("\n\nis\n\n");
				$equal=TRUE;
				foreach($replOne['ALL'] as $key=>$val) {
					if($txtArr[$start+$key]!=$val) {
						$equal=FALSE;
						break;
					}
				}
				if($equal) {
// 					echo("\n\nEQUAL\n\n");
					$bef=array_splice($txtArr,0,$start);
					$mid=array_splice($txtArr,0,count($replOne['ALL']));
					
					$results[]=implode(' ',$bef);
					$bef=array();
					$start=0;
// 					print_r($txtArr);
					
				}
// 				else echo("\n\nNOT EQUAL\n\n");

			}
			$start++;
			
		} while ($start < count($txtArr));
		
		// echo("remaining:\n");
		// print_r($txtArr);
		
		if(count($txtArr)>0) $results[]=implode(' ',$txtArr);
		
		return $results;
	}
}

$commander=new CommandUtils();

if(!function_exists('firstVal')) {function firstVal($arr) {foreach($arr as $v) return $v;} }
if(!function_exists('firstKey')) {function firstKey($arr) {foreach($arr as $k=>$v) return $k;} }


function isDomainAvailible($domain) {
	//check, if a valid url is provided
	if(!filter_var($domain, FILTER_VALIDATE_URL)) {
		return false;
	}
	//initialize curl
	$curlInit = curl_init($domain);
	curl_setopt($curlInit,CURLOPT_CONNECTTIMEOUT,2);
	curl_setopt($curlInit,CURLOPT_HEADER,true);
	curl_setopt($curlInit,CURLOPT_NOBODY,true);
	curl_setopt($curlInit,CURLOPT_RETURNTRANSFER,true);

	//get answer
	$response = curl_exec($curlInit);

	curl_close($curlInit);

	if ($response) return true;

	return false;
}

?>